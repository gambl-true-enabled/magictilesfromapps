package com.dabuterapp.appsmagics

import android.content.Context
import android.webkit.JavascriptInterface

private const val AUTH_TABLE = "com.auth.table.1232"
private const val AUTH_ARGS = "com.auth.value.3554"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(AUTH_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(AUTH_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(AUTH_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(AUTH_ARGS, null)
}
