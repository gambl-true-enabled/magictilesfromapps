package com.dabuterapp.appsmagics

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.magic_apps_fragment.*


class MagicAppsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.magic_apps_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainIntent = Intent(Intent.ACTION_MAIN, null)
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        pkgAppsList = context!!.packageManager.queryIntentActivities(mainIntent, 0)
        apps_wall.adapter = AppsAdapter(context!!, pkgAppsList, context!!.packageManager)
        apps_wall.onItemClickListener = gridViewOnItemClickListener
    }

    private lateinit var pkgAppsList: MutableList<ResolveInfo>
    private val gridViewOnItemClickListener = OnItemClickListener { _, _, position, _ ->
        openApp(context!!, pkgAppsList[position].activityInfo.packageName)
    }

    private fun openApp(context: Context, packageName: String): Boolean {
        val manager: PackageManager = context.packageManager
        return try {
            val i = manager.getLaunchIntentForPackage(packageName)
                ?: return false
            //throw new ActivityNotFoundException();
            i.addCategory(Intent.CATEGORY_LAUNCHER)
            context.startActivity(i)
            true
        } catch (e: ActivityNotFoundException) {
            false
        }
    }

}