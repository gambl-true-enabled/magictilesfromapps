package com.dabuterapp.appsmagics

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.onesignal.OneSignal

class App: Application() {
    private val conversionListener: AppsFlyerConversionListener = object : AppsFlyerConversionListener {
        override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
            conversionData.forEach {
                Log.i("VolcanoConversion1", "${it.key} ${it.value}")
            }
        }
        override fun onConversionDataFail(errorMessage: String) {
            Log.i("VolcanoConversion2", "onConversionDataFail $errorMessage")
        }
        override fun onAppOpenAttribution(attributionData: Map<String, String>) {
            attributionData.forEach {
                Log.i("VolcanoConversion3", "${it.key} ${it.value}")
            }
        }
        override fun onAttributionFailure(errorMessage: String) {
            Log.i("VolcanoConversion4", "onAttributionFailure $errorMessage")
        }
    }

    override fun onCreate() {
        super.onCreate()
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        AppsFlyerLib.getInstance().init("jzvY5PTmYwVkSNoTnySxYj", conversionListener, this)
        AppsFlyerLib.getInstance().startTracking(this)

        // Logging set to help debug issues, remove before releasing your app.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }

}