package com.dabuterapp.appsmagics

import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView


class AppsAdapter(
    private val mContext: Context,
    private val pkgAppsList: MutableList<ResolveInfo>,
    private val pckm: PackageManager
) : BaseAdapter() {

    override fun getCount(): Int {
        return pkgAppsList.size
    }

    override fun getItem(position: Int): Any? {
        return pkgAppsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup?
    ): View? {
        val grid = if (convertView == null) {
            val inflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            inflater.inflate(R.layout.magic_app_card, parent, false)
        } else {
            convertView
        }
        val imageView = grid.findViewById<View>(R.id.imagepart) as ImageView
        val textView = grid.findViewById<View>(R.id.textpart) as TextView
        imageView.setImageDrawable(pkgAppsList[position].loadIcon(pckm))
        textView.text = pkgAppsList[position].loadLabel(pckm)
        return grid
    }

}